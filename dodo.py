# -*- coding: utf-8 -*-
"""
Created on Tue May 21 19:48:41 2024

@author: lemer
"""

from typing import Union, Callable
import random
from generals_fonctions import grid_tuple_to_grid_list
from generals_fonctions import grid_list_to_grid_tuple
from generals_fonctions import pprint
from generals_fonctions import neighbours
from generals_fonctions import cell_value

# Types de base utilisés par l'arbitre
Environment = ...  # Ensemble des données utiles (cache, état de jeu...) pour
# que votre IA puisse jouer (objet, dictionnaire, autre...)
Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]  # case de départ -> case d'arrivée
Action = Union[ActionGopher, ActionDodo]
Player = int  # 1 ou 2
State = list[tuple[Cell, Player]]  # État du jeu pour la boucle de jeu
Score = int
Time = int

Strategy = Callable[[State, Player], ActionDodo]


def legals_cell(grid: State, player: Player, cell: Cell) -> list[ActionDodo]:
    '''Renvoie les actions légals pour une celulle d'un joueur'''

    # verification que la valeur de la celulle est bien celle du joueur
    c_value = cell_value(grid, cell)
    if c_value == player:

        size = int(len(grid)**0.5 // 2)
        neighbours_cells = neighbours(cell, size)

        legals_list = []

        for n in neighbours_cells:
            n_value = cell_value(grid, n)
            # verifier que le voisins est libre
            if n_value == 0:
                # si joueur 1 alors cellule en bas
                if player == 1 and (n[0] <= cell[0] and n[1] <= cell[1]):
                    legals_list.append((cell, n))
                # si joueur 2 alors cellule en haut
                elif player == 2 and (n[0] >= cell[0] and n[1] >= cell[1]):
                    legals_list.append((cell, n))

        return legals_list

    else:
        return []


def legals_player(grid: State, player: Player) -> list[ActionDodo]:
    '''Renvoie les actions légales d'un joueur'''

    legals_player_actions = []

    # parcours de toutes les cellules de la grille
    for c in grid:
        cell = c[0]
        if cell and c[1] == player:
            legals_cell_actions = legals_cell(grid, player, cell)
            legals_player_actions.extend(legals_cell_actions)

    return legals_player_actions


def final(grid: State) -> bool:
    '''Renvoie si le jeu est termine'''

    # boucle sur les joueurs
    for player in range(1, 3):

        # renvoie True si aucune actions disponible pour le joueur
        legals_player_actions = legals_player(grid, player)
        if len(legals_player_actions) == 0:
            return True

    # renvoie False sinon
    return False


def evaluation(grid: State) -> float:
    '''Renvoie une evaluation du jeu
    A changer
    Evaluation pas du tout reflechis (fait en 5min)'''

    player_moves = []
    for player in range(1, 3):
        legals_player_actions = legals_player(grid, player)
        player_moves.append(len(legals_player_actions))

    m1, m2 = player_moves[0], player_moves[1]

    # 1er cas : les 2 joueurs ont autant de coup possible
    if m1 == m2:
        return 0

    # 2eme cas : j1 a moins de coup que j2
    elif m1 < m2:
        return m2 / (m1+m2)

    # 3eme cas : j2 a moins de coup de j1
    else:
        return m1 / (m1+m2)


def score(grid: State) -> float:
    '''Renvoie le score du jeu'''

    # si etat final
    # renvoie 1 si joueur 1 gagne
    # renvoie 2 si joueur 2 gagne
    for player in range(1, 3):
        legals_player_actions = legals_player(grid, player)
        if len(legals_player_actions) == 0 and player == 1:
            return 1
        elif len(legals_player_actions) == 0 and player == 2:
            return -1

    # si le jeu n'est pas finis, on renvoie l'evaluation du score
    return evaluation(grid)


def play(grid: State, player: Player, action: ActionDodo) -> State:
    '''Joue l'action d'un joueur sur une grille'''

    # conversion de la grille en liste
    grid_list = grid_tuple_to_grid_list(grid)

    # position
    position = action[0]
    new_position = action[1]

    # booleen pour verifier que les cellules sont bien voisines
    neighbours_bool = False
    # booléen pour verifier que les positions indiqués sont bien valable
    position_bool = False
    new_position_bool = False

    position_neighbours = legals_cell(grid, player, position)
    if action in position_neighbours:
        neighbours_bool = True

    for i, cell in enumerate(grid_list):
        # si on trouve la position initial
        if cell[0] == list(position) and cell[1] == player:
            position_index = i
            position_bool = True

        # si on trouve la nouvelle position
        if cell[0] == list(new_position) and cell[1] == 0:
            new_position_index = i
            new_position_bool = True

    # si bool alors application de l'action
    if position_bool and new_position_bool and neighbours_bool:
        grid_list[position_index][1] = 0
        grid_list[new_position_index][1] = player

    else:
        print("Coup non valide.")

    return grid_list_to_grid_tuple(grid_list)


def strategy_random(grid: State, player: Player) -> ActionDodo:
    '''Strategie aleatoire'''

    legals_player_actions = legals_player(grid, player)
    random_index = random.randint(0, len(legals_player_actions)-1)
    return legals_player_actions[random_index]


def dodo(grid: State, strategy_1: Strategy, strategy_2: Strategy, debug: bool = False) -> Score:

    current_player = 1

    while True:
        if current_player == 1:
            action = strategy_1(grid, 1)
        else:
            action = strategy_2(grid, 2)

        if action not in legals_player(grid, current_player):
            if debug:
                print('Coup invalide', action)
            continue

        grid = play(grid, current_player, action)

        if debug:
            print(f'Player {current_player} played at position {action}:')

        # si partie termine
        if final(grid):
            winner = score(grid)
            if debug:
                print('Gagnant : ', winner)
                pprint(grid)
            return winner

        # si partie n'est pas termine
        current_player = 2 if current_player == 1 else 1


# exemple d'une grille de taille 2
grid_2 = [(None, None), (None, None), ((0, 2), 1), ((1, 2), 1), ((2, 2), 1),
          (None, None), ((-1, 1), 0), ((0, 1), 0), ((1, 1), 1), ((2, 1), 1),
          ((-2, 0), 2), ((-1, 0), 0), ((0, 0), 0), ((1, 0), 0), ((2, 0), 1),
          ((-2, -1), 2), ((-1, -1), 2), ((0, -1), 0), ((1, -1), 0), (None, None),
          ((-2, -2), 2), ((-1, -2), 2), ((0, -2), 2), (None, None), (None, None)]

grid_2_final = [(None, None), (None, None), ((0, 2), 2), ((1, 2), 2), ((2, 2), 1),
                (None, None), ((-1, 1), 0), ((0, 1), 0), ((1, 1), 2), ((2, 1), 2),
                ((-2, 0), 1), ((-1, 0), 0), ((0, 0), 2), ((1, 0), 0), ((2, 0), 2),
                ((-2, -1), 1), ((-1, -1), 0), ((0, -1), 0),
                ((1, -1), 0), (None, None),
                ((-2, -2), 1), ((-1, -2), 1), ((0, -2), 1),
                (None, None), (None, None)]

# grille de taille 3
# c'est la grille initiale dans le regle du jeu (fig. 1)
grid_3 = [(None, None), (None, None), (None, None),
          ((0, 3), 1), ((1, 3), 1), ((2, 3), 1), ((3, 3), 1),
          (None, None), (None, None),
          ((-1, 2), 0), ((0, 2), 1), ((1, 2), 1), ((2, 2), 1), ((3, 2), 1),
          (None, None),
          ((-2, 1), 0), ((-1, 1), 0), ((0, 1), 0), ((1, 1), 1), ((2, 1), 1),
          ((3, 1), 1),
          ((-3, 0), 2), ((-2, 0), 2), ((-1, 0), 0), ((0, 0), 0), ((1, 0), 0),
          ((2, 0), 1), ((3, 0), 1),
          ((-3, -1), 2), ((-2, -1), 2), ((-1, -1), 2), ((0, -1), 0),
          ((1, -1), 0), ((2, -1), 0), (None, None),
          ((-3, -2), 2), ((-2, -2), 2), ((-1, -2), 2), ((0, -2), 2),
          ((1, -2), 0), (None, None), (None, None),
          ((-3, -3), 2), ((-2, -3), 2), ((-1, -3), 2), ((0, -3), 2),
          (None, None), (None, None), (None, None)]

# grille intermediaire dans le regle du jeu (fig. 2)
grid_3_inter = [(None, None), (None, None), (None, None),
                ((0, 3), 0), ((1, 3), 1), ((2, 3), 1), ((3, 3), 1),
                (None, None), (None, None),
                ((-1, 2), 1), ((0, 2), 2), ((1, 2), 1), ((2, 2), 0), ((3, 2), 1),
                (None, None),
                ((-2, 1), 2), ((-1, 1), 1), ((0, 1), 0), ((1, 1), 1), ((2, 1), 0),
                ((3, 1), 1),
                ((-3, 0), 0), ((-2, 0), 2), ((-1, 0), 2), ((0, 0), 1), ((1, 0), 2),
                ((2, 0), 2), ((3, 0), 0),
                ((-3, -1), 2), ((-2, -1), 2), ((-1, -1), 0), ((0, -1), 0),
                ((1, -1), 0), ((2, -1), 1), (None, None),
                ((-3, -2), 2), ((-2, -2), 0), ((-1, -2), 1), ((0, -2), 2),
                ((1, -2), 1), (None, None), (None, None),
                ((-3, -3), 2), ((-2, -3), 2), ((-1, -3), 0), ((0, -3), 2),
                (None, None), (None, None), (None, None)]

# grille finale dans le regle du jeu (fig. 3)
grid_3_final = [(None, None), (None, None), (None, None),
                ((0, 3), 2), ((1, 3), 2), ((2, 3), 2), ((3, 3), 2),
                (None, None), (None, None),
                ((-1, 2), 0), ((0, 2), 0), ((1, 2), 2), ((2, 2), 2), ((3, 2), 1),
                (None, None),
                ((-2, 1), 1), ((-1, 1), 0), ((0, 1), 2), ((1, 1), 2), ((2, 1), 2),
                ((3, 1), 1),
                ((-3, 0), 1), ((-2, 0), 1), ((-1, 0), 0), ((0, 0), 0), ((1, 0), 2),
                ((2, 0), 2), ((3, 0), 2),
                ((-3, -1), 1), ((-2, -1), 1), ((-1, -1), 0), ((0, -1), 0),
                ((1, -1), 0), ((2, -1), 0), (None, None),
                ((-3, -2), 1), ((-2, -2), 1), ((-1, -2), 1), ((0, -2), 0),
                ((1, -2), 2), (None, None), (None, None),
                ((-3, -3), 1), ((-2, -3), 1), ((-1, -3), 1), ((0, -3), 0),
                (None, None), (None, None), (None, None)]


a = dodo(grid_2, strategy_random, strategy_random, debug=True)
